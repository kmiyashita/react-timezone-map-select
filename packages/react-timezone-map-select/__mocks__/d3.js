// d3 is automatically mocked.
// See https://jestjs.io/docs/manual-mocks#mocking-node-modules

function pathGenerator() {
    return undefined;
}

function geoPath() {
    return pathGenerator;
}

module.exports = { geoPath };