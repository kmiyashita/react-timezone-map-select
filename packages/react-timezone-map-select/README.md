# react-timezone-map-select

[![pipeline status](https://gitlab.com/kmiyashita/react-timezone-map-select/badges/main/pipeline.svg?ignore_skipped=true)](https://gitlab.com/kmiyashita/react-timezone-map-select/-/commits/main) [![coverage report](https://gitlab.com/kmiyashita/react-timezone-map-select/badges/main/coverage.svg?job=unit-test-job)](https://gitlab.com/kmiyashita/react-timezone-map-select/-/commits/main) [![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)


## Features

This is a react component which lets you select a timezone on world map in a modal dialog.

- Modal dialog is implemented using [MUI (a.k.a. material UI) ver 5.x](https://mui.com/).
- You can select a timezone from the list of country / city.
- You can also select a country / city on the world map.
- It is fully customizable to make it fit to your application's look and feel.
  - All text labels
  - Button design (you can specify your own react component)
  - Order of buttons.  (Mac style: OK comes last.  Windows style: OK comes first.)

![modal dialog screenshot](https://gitlab.com/kmiyashita/react-timezone-map-select/uploads/bc971f68f3f3a8fdfa96f6cfb0218795/image.png)


## Installation

`react-timezone-map-select` is available as an [npm package](https://www.npmjs.com/package/react-timezone-map-select).

To install and save in your `package.json` dependencies, run:

```
// with npm
npm install react-timezone-map-select

// with yarn
yarn add react-timezone-map-select
```

## Usage

You can use TimeZoneSelectDialog in the same manner as [modal dialog](https://mui.com/components/dialogs/) in MUI.

```typescript
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import React, { ReactElement } from 'react';
import { TimeZoneSelectDialog } from 'react-timezone-map-select';


function App(): ReactElement {
  /** Timezone name */
  const [timezoneName, setTimezoneName] = React.useState('America/Los_Angeles');

  /** Set true when you open TimeZoneSelectDialog. */
  const [open, setOpen] = React.useState(false);

  /** Called when you press "Open Dialog" button. */
  const handleOpen = React.useCallback(() => {
    setOpen(true);
  }, []);

  /** Called when TimeZoneSelectDialog is closed. */
  const handleClose = React.useCallback((newTimeZoneName: string) => {
    setTimezoneName(newTimeZoneName);
    setOpen(false);
  }, []);

  return (
    <Container>
      <Box>
        <p>Timezone = {timezoneName}</p>
      </Box>
      <Box>
        <Button onClick={handleOpen} variant="contained" >
          Open Dialog
        </Button>
      </Box>
      <TimeZoneSelectDialog
        open={open}
        timeZoneName={timezoneName}
        onClose={handleClose}
      />
    </Container>
  );
}
```

## Examples

### Simple demo

- [Live demo](https://kmiyashita.gitlab.io/react-timezone-map-select/simple-demo/)
- [Source code](https://gitlab.com/kmiyashita/react-timezone-map-select/-/tree/main/examples/simple-demo)

### Customize demo

- [Live demo](https://kmiyashita.gitlab.io/react-timezone-map-select/customize-demo/)
- [Source code](https://gitlab.com/kmiyashita/react-timezone-map-select/-/tree/main/examples/customize-demo)

## Properties and Functions

### TimeZoneSelectDialog

| Property name          | Type                                             | Default Value                                                 | Description                                                                                           |
|------------------------|--------------------------------------------------|---------------------------------------------------------------|-------------------------------------------------------------------------------------------------------|
| open                   | boolean                                          | false                                                         | If true, the modal dialog is shown.                                                                   |
| onClose                | (newTZ:string) => void                           | undefined                                                     | Callback fired when the modal dialog is to be closed.                                                 |
| timezoneName           | string                                           | "America/Los_Angeles"                                         | Initial timezone name.                                                                                |
| title                  | string                                           | "Time Zone"                                                   | Title string.                                                                                         |
| description            | string                                           | "Select a time zone in the list or click an area on the map." | Description string.                                                                                   |
| buttonLabelOk          | string                                           | "OK"                                                          | Label on "ok" button.                                                                                 |
| buttonLabelCancel      | string                                           | "Cancel"                                                      | Label on "cancel" button.                                                                             |
| SubstituteButtonOk     | ((props: ButtonProps) => ReactElement) \| string; | @mui/material/Button                                          | React component to be used as "ok" button.                                                            |
| SubstituteButtonCancel | ((props: ButtonProps) => ReactElement) \| string; | @mui/material/Button                                          | React component to be used as "cancel" button.                                                        |
| isOkFirstButton        | boolean                                          | false                                                         | If true, "ok" button comes first (Windows style). Otherwise, "cancel" button comes first (Mac style). |

### convertOffsetInMinutesToString

```typescript
function convertOffsetInMinutesToString(offsetInMinutes: number): string
```

Convert time offset to user friendly string.  e.g.  -60 ==> "-01:00"

Parameters
| Parameter  | Type   |  Description   |
|------------|--------|----------------|
| offsetInMinutes  | number | offset from UTC in minutes. |

Return value

User friendly string e.g. "-01:00" 

### findTimeZone

```typescript
function findTimeZone(timeZoneName: string): RawTimeZone | undefined
```

Find a time zone data in `@vvo/tzdb`.

Parameters

| Parameter  | Type   |  Description   |
|------------|--------|----------------|
| timeZoneName | string | Time zone name. Note it could be grouped in "group". |

Return value

Time zone data in `@vvo/tzdb` if found. undefined if not found.


## License

react-timezone-map-select is available under the MIT License.
